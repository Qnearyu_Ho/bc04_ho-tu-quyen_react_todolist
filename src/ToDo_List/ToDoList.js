import React, { Component } from "react";
import { Container } from "../Components_ToDoList/Container";
import { ThemeProvider } from "styled-components";
import { ToDoListDarkTheme } from "../Themes/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../Themes/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../Themes/ToDoListPrimaryTheme";
import { Dropdown } from "../Components_ToDoList/Dropdown";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "../Components_ToDoList/Heading";
import { TextField, Label, Input } from "../Components_ToDoList/TextField";
import { Button } from "../Components_ToDoList/Button";
import { Table, Thead, Th, Tr } from "../Components_ToDoList/Table";
import { connect } from "react-redux";
import {
  addTaskAction,
  changeThemeAction,
  doneTaskAction,
  deleteTaskAction,
  editTaskAction,
  updateTask,
} from "../Redux/actions/ToDoListActions";
import { arrTheme } from "../Themes/ThemeManager";
class ToDoList extends Component {
  state = {
    taskName: "",
    disabled: true,
  };
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle", textAlign: "left" }}>
              {task.taskName}
            </Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.setState(
                    {
                      disabled: false,
                    },
                    () => {
                      this.props.dispatch(editTaskAction(task));
                    }
                  );
                }}
                className="ml-1"
              >
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr>
            <Th style={{ verticalAlign: "middle", textAlign: "left" }}>
              {task.taskName}
            </Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  // Viết hàm render Theme import ThemeManager
  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };
  // Life cycle
  // componentWillReceiveProps(newProps) {
  //   console.log("this.props", this.props);
  //   console.log("newProps", newProps);
  //   this.setState({
  //     taskName: newProps.taskEdit.taskName,
  //   });
  // }

  // Life Cycle tĩnh không truy xuất được trỏ this
  // static getDerivedStateFromProps(newProps, currentState) {
  //   // newProps: la Props moi, prop cu la this.props (khong truy xuat duoc)
  //   // currentState: ung voi State hien tai

  //   // hoac tra ve state moi
  //   let newState = { ...currentState, taskName: newProps.taskEdit.taskName };
  //   return newState;
  //   // tra ve null state giu nguyen
  //   // return null;
  // }
  render() {
    return (
      <div>
        <ThemeProvider theme={this.props.themeToDoList}>
          <Container className="w-50">
            <Dropdown
              onChange={(e) => {
                let { value } = e.target;
                // Dispatch value len reducer
                this.props.dispatch(
                  // type: "change_theme",
                  // themeId: value,
                  changeThemeAction(value)
                );
              }}
            >
              {/* <option>Dark theme</option>
              <option>Light theme</option>
              <option>Primary theme</option> */}
              {this.renderTheme()}
            </Dropdown>
            <Heading3>To Do List</Heading3>
            <TextField
              value={this.state.taskName}
              onChange={(e) => {
                this.setState(
                  {
                    taskName: e.target.value,
                    // or
                    // handleChange = (e){
                    //   let {name, value}= e.target.value;
                    //   this.setState({
                    //     [name]:value
                    //   })
                    // }
                  },
                  () => {
                    console.log(this.state);
                  }
                );
              }}
              label="Task name"
              style={{ textAlign: "left" }}
              className="w-50"
            ></TextField>
            <Button
              onClick={() => {
                // lấy thông tin người dùng nhập vào từ input
                let { taskName } = this.state;
                // Tạo ra 1 task object
                let newTask = {
                  id: Date.now(),
                  taskName: taskName,
                  done: false,
                };
                console.log(newTask);
                // Đưa task object lên redux thông qua phương thức dispatch

                this.props.dispatch(addTaskAction(newTask));
              }}
              className="ml-2"
            >
              <i className="fa fa-plus"></i>Add task
            </Button>
            {this.state.disabled ? (
              <Button
                disabled
                onClick={() => {
                  this.props.dispatch(updateTask(this.state.taskName));
                }}
                className="ml-2"
              >
                <i className="fa fa-upload"></i>Update task
              </Button>
            ) : (
              <Button
                onClick={() => {
                  this.setState(
                    {
                      disabled: false,
                    },
                    () => this.props.dispatch(updateTask(this.state.taskName))
                  );
                }}
                className="ml-2"
              >
                <i className="fa fa-upload"></i>Update task
              </Button>
            )}
            <hr />
            <Heading3>Task to do</Heading3>
            <Table>
              <Thead>
                {/* <Tr>
                  <Th style={{ verticalAlign: "middle", textAlign: "left" }}>
                    Task name
                  </Th>
                  <Th className="text-right">
                    <Button className="ml-1">
                      <i className="fa fa-edit"></i>
                    </Button>
                    <Button className="ml-1">
                      <i className="fa fa-check"></i>
                    </Button>
                    <Button className="ml-1">
                      <i className="fa fa-trash"></i>
                    </Button>
                  </Th>
                </Tr>
                <Tr>
                  <Th style={{ verticalAlign: "middle", textAlign: "left" }}>
                    Task name
                  </Th>
                  <Th className="text-right">
                    <Button className="ml-1">
                      <i className="fa fa-edit"></i>
                    </Button>
                    <Button className="ml-1">
                      <i className="fa fa-check"></i>
                    </Button>
                    <Button className="ml-1">
                      <i className="fa fa-trash"></i>
                    </Button>
                  </Th>
                </Tr> */}
                {this.renderTaskToDo()}
              </Thead>
            </Table>
            <Heading3>Task Completed</Heading3>
            <Table>
              <Thead>{this.renderTaskCompleted()}</Thead>
            </Table>
          </Container>
        </ThemeProvider>
      </div>
    );
  }
  // Đây là Life Cycle trả về props cũ và state cũ của component trước khi render
  // Life cycle này chạy sau render
  componentDidUpdate(prevProps, prevState) {
    // So sánh nếu như props trước đó (taskEdit trước đó khác taskEdit hiện tại
    // thì mới setState)
    if (prevProps.taskEdit.id !== this.props.taskEdit.id)
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.todolistReducer.themeToDoList,
    taskList: state.todolistReducer.taskList,
    taskEdit: state.todolistReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);
